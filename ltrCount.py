import sys
import re
textFile = sys.argv[1]
with open(textFile, 'r') as f:
    text = f.read()
    text = text.split(" ")
    wordStr = ""
    
    for word in text:
        cleanWord = word.strip()
        punctWord = re.sub(r'[^\W]+', "",  cleanWord)
        countWord = re.sub(r'\W+', "", cleanWord)
        wordLen =  len(countWord)
        if cleanWord[-1] in punctWord:
            wordStr += cleanWord[:-1] + "(" + str(wordLen) + ")" + cleanWord[-1] + " "
        else:
            wordStr += cleanWord + "(" + str(wordLen) +") "
    wordStr = wordStr.strip()
    print wordStr
