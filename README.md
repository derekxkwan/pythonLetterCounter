# pythonLetterCounter
Counts the number of letters (not including punctuation) using Python 2.7.

How To Call:
$ python ltrCount.py inputText.txt

Sample Output:
Hello(5), this(4) is(2) a(1) demo(4) of(2) ltrCount.py(10)! The(3) count(5) doesn't(6) include(7) punctuation(11) (but(3) does(4) include(7) numbers(7)) and(3) returns(7) the(3) input(5) textfile(8) (the(3) second(6) argument(8)) with(4) the(3) number(6) of(2) letters(7) in(2) parentheses(11) next(4) to(2) the(3) word(4).
